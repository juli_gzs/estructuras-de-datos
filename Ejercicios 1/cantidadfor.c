#include <stdio.h>

/* Cuenta la cantidad de digitos de un numero con for*/

int main(){
	
	int contador = 0;
	int numero;
	
	printf("Escriba un número:");
	scanf("%d", &numero);
	
	for(contador = 0; numero > 0; contador++){
		numero = numero / 10;
	}
	
	printf("%d", contador);
}
