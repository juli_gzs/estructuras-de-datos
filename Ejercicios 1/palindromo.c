#include <stdio.h>

/* Determina si un numero es palindromo */

int main(){
	
	int inicial = 0;
	int numero = 0;
	int invertido = 0;

	printf("Escriba un numero:");
	scanf("%d", &numero);
	inicial = numero;

	while(numero != 0){
		invertido = invertido * 10;
		invertido = invertido + numero % 10;
		numero = numero / 10;
	}

	if(inicial == invertido){
		printf("Es palindromo");
	}
	
	else{
		printf("No es palindromo");
	}
	
	return 0;
}
