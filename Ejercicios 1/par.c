#include <stdio.h>

/* Determina si un numero es par o impar */

int main(){
	
	int numero;
  
	printf("Escriba un número:");
	scanf("%d", &numero);
  
	if(numero % 2 == 0){
		printf("Par");
	} 
	else{
		printf("Impar");
	}
  
	return 0;
}
