#include <stdio.h>

/* Imprime el factorial de un numero */

int main(){
	
	int numero = 0;
	int i = 0;
	int factorial = 1;

	printf("Escriba un numero:");
	scanf("%d", &numero);

	for(i = 1; i <= numero; i++){
		factorial *= i;
	}
	printf("El factorial de %d es %d", numero, factorial);
}
