#include <stdio.h>

/* Invierte un numero */

int main(){
	
	int numero = 0;
	int invertido = 0;

	printf("Escriba un numero:");
	scanf("%d", &numero);

	while(numero != 0){
		invertido = invertido * 10;
		invertido = invertido + numero % 10;
		numero = numero / 10;
	}

	printf("%d", invertido);

	return 0;
}
