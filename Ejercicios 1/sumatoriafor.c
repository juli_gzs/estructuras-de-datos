#include <stdio.h>

/* Sumatoria de cero a n con for*/

int main(){
	
	int total = 0;
	int suma;
	int n;
	
	printf("Escriba un número:");
	scanf("%d", &n);
	
	for(suma = 0; n >= 0; n--){
		total = total + suma;
		suma = suma + 1;
	}
	
	printf("%d", total);
}
