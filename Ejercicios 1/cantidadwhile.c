#include <stdio.h>

/* Cuenta la cantidad de digitos de un numero con while*/

int main(){
	
	int contador = 0;
	int numero;
	
	printf("Escriba un número:");
	scanf("%d", &numero);
	
	while(numero != 0){
		numero = numero / 10;
		contador ++;
	}
	
	printf("%d", contador);
}
