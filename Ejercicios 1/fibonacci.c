#include <stdio.h>

/* Imprime la cantidad de numeros deseada de la secuencia de Fibonacci */

int main(){
	
	int cantidad;
	int valor0 = 1;
	int valor1 = 1;
	int valor2 = 1;
	int valor3 = valor1 + valor2;
	
	printf("Cuantos numeros de Fibonacci desea ver?");
	scanf("%d", &cantidad);
	
	if(cantidad == 0){
		printf("%d", cantidad);
	}
	else if(cantidad == 1){
		printf("%d", cantidad);
	}
	else if(cantidad == 2){
		printf("%d,", cantidad - 1);
		printf("%d", cantidad - 1);
	}
	else{
		printf("%d,", valor0);
		printf("%d,", valor0);
		while(cantidad > 2){
			printf("%d,", valor3);
			valor1 = valor2;
			valor2 = valor3;
			valor3 = valor1 + valor2;
			cantidad --;
		}
	}
}
