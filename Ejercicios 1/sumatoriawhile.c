#include <stdio.h>

/* Sumatoria de cero a n con while*/

int main(){
	
	int total = 0;
	int suma = 0;
	int n;
	
	printf("Escriba un número:");
	scanf("%d", &n);
	
	while(n >= 0){
		total = total + suma;
		suma = suma + 1;
		n --;
	}
	
	printf("%d", total);
}
